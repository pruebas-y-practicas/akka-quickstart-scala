package com.example.iot

import akka.actor.ActorSystem
import com.example.MainTrait

import scala.io.StdIn

object IotApp extends MainTrait{
  override val systemName: Option[String] = Some("iot-system")

  def executeThis(): Unit = {
    try {
      // Create top level supervisor
      val supervisor = system.actorOf(IotSupervisor.props(), "iot-supervisor")
      // Exit the system after ENTER is pressed
      StdIn.readLine()
    } finally {
      system.terminate()
    }
  }

}