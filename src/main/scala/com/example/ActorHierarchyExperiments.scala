package com.example

import akka.actor.{ Actor, ActorSystem, Props }
import scala.io.StdIn

object PrintMyActorRefActor {
  def props: Props =
    Props(new PrintMyActorRefActor)
}

class PrintMyActorRefActor extends Actor {
  override def receive: Receive = {
    case "printit" =>
      val secondRef = context.actorOf(Props.empty, "second-actor")
      println(s"Second: $secondRef")
  }
}

object ActorHierarchyExperiments extends MainTrait {

  override def executeThis(): Unit = {
    val firstRef = system.actorOf(PrintMyActorRefActor.props, "first-actor")
    println(s"First: $firstRef")
    firstRef ! "printit"
  }
}