package com.example

import akka.actor.ActorSystem
import scala.io.StdIn

trait MainTrait extends App {
  val systemName: Option[String] = None
  val system = ActorSystem(systemName.getOrElse("testSystem"))

  executeThis()

  println(">>> Press ENTER to exit <<<")
  try StdIn.readLine()
  finally system.terminate()

  def executeThis(): Unit
}
