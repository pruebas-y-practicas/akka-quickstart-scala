package com.example

import akka.actor.{Actor, Props}

object FailureHandlingActors extends MainTrait {

  override def executeThis(): Unit = {
    val supervisingActor = system.actorOf(SupervisingActor.props, "supervising-actor")
    supervisingActor ! "failChild"
  }
}

object SupervisingActor {
  def props: Props =
    Props(new SupervisingActor)
}

class SupervisingActor extends Actor {
  val child = context.actorOf(SupervisedActor.props, "supervised-actor")

  override def receive: Receive = {
    case "failChild" => child ! "fail"
  }
}

object SupervisedActor {
  def props: Props =
    Props(new SupervisedActor)
}

class SupervisedActor extends Actor {
  override def preStart(): Unit = println("supervised actor started")
  override def postStop(): Unit = println("supervised actor stopped")
  override def postRestart(reason: Throwable): Unit = println("supervised actor post-restarted")
  override def preRestart(reason: Throwable, message: Option[Any]): Unit = println("supervised actor pre-restarted")

  override def receive: Receive = {
    case "fail" =>
      println("supervised actor fails now")
      throw new Exception("I failed!")
  }
}
